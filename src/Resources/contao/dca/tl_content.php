<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;

$GLOBALS['TL_DCA']['tl_content']['fields']['imgFluid'] = [
  'exclude'                   => true,
  'inputType'                 => 'checkbox',
  'eval'                      => array('submitOnChange' => true, 'tl_class' => 'w50 clr m12'),
  'sql'                       => "char(1) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['imgAlignment'] = [
  'exclude'                   => true,
  'inputType'                 => 'select',
  'options'                   => array(
                                  'img-center','img-left','img-right',
                                  'sm:img-center', 'sm:img-left', 'sm:img-right',
                                  'md:img-center', 'md:img-left', 'md:img-right',
                                  'lg:img-center', 'lg:img-left', 'lg:img-right',
                                  'xl:img-center', 'xl:img-left', 'xl:img-right'),
  'eval'                      => ['includeBlankOption'=>true, 'tl_class'=>'w50', 'size'=>'10', 'multiple'=>true, 'chosen'=>true],
  'sql'                       => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['imgRadius'] = [
  'exclude'                   => true,
  'inputType'                 => 'select',
  'options'                   => array('rounded-sm','rounded','rounded-md','rounded-lg','rounded-xl','rounded-full'),
  'eval'                      => ['includeBlankOption'=>true, 'tl_class'=>'w50'],
  'sql'                       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['imgObjectFit'] = [
  'exclude'                   => true,
  'inputType'                 => 'select',
  'options'                   => array('img-cover','sm:img-cover','md:img-cover','lg:img-cover','xl:img-cover'),
  'eval'                      => ['includeBlankOption'=>true, 'tl_class'=>'w50', 'size'=>'10', 'multiple'=>true, 'chosen'=>true],
  'sql'                       => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['imgAspect'] = [
  'exclude'                   => true,
  'inputType'                 => 'select',
  'options'                   => array(
                                  'ratio-1610','ratio-169','ratio-54','ratio-45','ratio-43','ratio-34','ratio-32','ratio-23','ratio-31','ratio-21','ratio-12','ratio-11','ratio-none',
                                  'sm:ratio-1610','sm:ratio-169','sm:ratio-54','sm:ratio-45','sm:ratio-43','sm:ratio-34','sm:ratio-32','sm:ratio-23','sm:ratio-31','sm:ratio-21','sm:ratio-12','sm:ratio-11','sm:ratio-none',
                                  'md:ratio-1610','md:ratio-169','md:ratio-54','md:ratio-45','md:ratio-43','md:ratio-34','md:ratio-32','md:ratio-23','md:ratio-31','md:ratio-21','md:ratio-12','md:ratio-11','md:ratio-none',
                                  'lg:ratio-1610','lg:ratio-169','lg:ratio-54','lg:ratio-45','lg:ratio-43','lg:ratio-34','lg:ratio-32','lg:ratio-23','lg:ratio-31','lg:ratio-21','lg:ratio-12','lg:ratio-11','lg:ratio-none',
                                  'xl:ratio-1610','xl:ratio-169','xl:ratio-54','xl:ratio-45','xl:ratio-43','xl:ratio-34','xl:ratio-32','xl:ratio-23','xl:ratio-31','xl:ratio-21','xl:ratio-12','xl:ratio-11','xl:ratio-none',),
  'eval'                      => ['includeBlankOption'=>true, 'tl_class'=>'w50', 'size'=>'10', 'multiple'=>true, 'chosen'=>true],
  'sql'                       => "text NULL"
];

PaletteManipulator::create()
  ->addField('imgFluid', 'source_legend', PaletteManipulator::POSITION_APPEND)
  ->addField('imgAspect', 'source_legend', PaletteManipulator::POSITION_APPEND)
  ->addField('imgAlignment', 'source_legend', PaletteManipulator::POSITION_APPEND)
  ->addField('imgObjectFit', 'source_legend', PaletteManipulator::POSITION_APPEND)
  ->addField('imgRadius', 'source_legend', PaletteManipulator::POSITION_APPEND)
  ->applyToPalette('image', 'tl_content')
  ->applyToSubpalette('addImage', 'tl_content')
;
