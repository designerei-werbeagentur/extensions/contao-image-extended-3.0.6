<?php

// src/EventListener/ParseTemplateListener.php
namespace designerei\ContaoImageExtendedBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Template;

/**
 * @Hook("parseTemplate")
 */
class ContaoImageExtendedListener
{
    public function __invoke(Template $template): void
    {

        // fluid display
        if($template->imgFluid)
        {
            $template->imgClass .= ' w-auto';
        }

        // image alignment
        if($template->imgAlignment)
        {
            $template->imgAlignment = deserialize($template->imgAlignment);
            foreach($template->imgAlignment as $alignmentClass)
            {
                $template->imgClass .= ' ' . $alignmentClass;
            }
        }

        // image radius
        if($template->imgRadius)
        {
            $template->imgClass .= ' ' . $template->imgRadius;

        }

        // image radius
        if($template->imgObjectFit)
        {
            $template->imgObjectFit = deserialize($template->imgObjectFit);
            foreach($template->imgObjectFit as $objectFitClass)
            {
                $template->imgClass .= ' ' . $objectFitClass;
            }
        }

        // trim imgClass
        $template->imgClass = trim($template->imgClass);
        $template->imgClass = implode(' ', array_unique(explode(' ', $template->imgClass)));

        // image aspect ratio
        if($template->imgAspect)
        {
            $template->imgAspect = deserialize($template->imgAspect);
            foreach($template->imgAspect as $imgAspectClass)
            {
                $template->aspectClass .= ' ' . $imgAspectClass;
            }

            $template->aspectClass = trim($template->aspectClass);
        }

        // trim aspectClass
        $template->aspectClass = trim($template->aspectClass);
        $template->aspectClass = implode(' ', array_unique(explode(' ', $template->aspectClass)));
    }
}
