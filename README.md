# Contao-Image-Extended

## About

This extension for Contao Open Source CMS extended the default image content element and is customized to work together with [Conplate-Starterkit](https://gitlab.com/designerei-werbeagentur/conplate/conplate-starterkit).

## Functions

- Fluid display: Reset the predefined CSS settings for responsive images.
- Image alignment: set the alignment of the image, if the image don't cover the complete width of the parent element
- Rounded corners: select the radius of the rounded corners
- Aspect ratio: change the aspect ratio of the image

Also it is extended with additional TailwindCSS classes and components to work together with [Conplate-Starterkit](https://gitlab.com/designerei-werbeagentur/conplate/conplate-starterkit). For a futher look, check out the styles within the file: [tailwind.config.js](https://gitlab.com/designerei-werbeagentur/conplate/conplate-starterkit/-/blob/master/tailwind.config.js).
